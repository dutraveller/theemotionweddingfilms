<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>The Emotion Wedding Films</title>
        <meta name="description" content="A Emotion Wedding Films faz filmagens de casamento com alma.">
    	<meta name="author" content="Tiago Milheiro">
        <meta property="og:url" content="http://theemotionweddingfilms.com/"/>
    	<meta property="og:type" content="website"/>
    	<meta property="og:title" content="The Emotion Wedding Films"/>
    	<meta property="og:image" content="http://theemotionweddingfilms.com/img/logo_2.png"/>
    	<meta property="og:description" content="A Emotion Wedding Films faz filmagens de casamento com alma. "/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="robots" content="noindex, nofollow">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300|Quicksand:300" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <header>
            <div class="header-container">
                <div class="wrapper">
                    <div class="header-logo">
                        <img src="img/logo.png" alt="" width="218">
                    </div>
                    <div class="header-nav">
                        <nav>
                            <ul>
                                <li><a href="sobre.html" rel="author">Sobre nós</a></li>
                                <li><a href="#realweddings">Real Weddings</a></li>
                                <li><a href="#servicos">Serviços</a></li>
                                <li><a href="#contactos">Contactos</a></li>
                                <li><span>PT</span> | <a class="language" href="/en/index.html"></a>EN</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <section class="intro">
            <div class="container">
                <h1>"In dreams and in love there are no impossibilities"</h1>
            </div>
        </section>
        <section class="showcase" id="realweddings">
            <div class="container">
                <h2 class="container__title">real weddings</h2>
                <div class="wrapper">
                    <div class="showcase__item">
                        <figure>
                            <img src="" alt="">
    						<figcaption>Same Day Edit - Marta & Daniel</figcaption>
    					</figure>
                    </div>
                    <div class="showcase__item">
                        <figure>
                            <img src="" alt="">
    						<figcaption>Same Day Edit - Marta & Daniel</figcaption>
    					</figure>
                    </div>
                    <div class="showcase__item">
                        <figure>
                            <!--<iframe src="http://player.vimeo.com/video/100305057?title=0&amp;byline=0&amp;portrait=0" frameborder="0" width="320" height="205"></iframe>-->
    						<figcaption>Same Day Edit - Marta & Daniel</figcaption>
    					</figure>
                    </div>
                </div>
            </div>
        </section>
        <section class="servicos" id="servicos">
            <div class="servicos__header">
                <span class="circle"></span>
                <span class="circle--2"></span>
                <span class="circle--3"></span>
                <span class="circle--4"></span>
                <h2 class="container__title">Os nossos serviços</h2>
            </div>
            <div class="container">
                <div class="wrapper">
                    <div class="show servicos__item item_1">
                        <img src="img/image-1.png" alt="">
                    </div>
                    <div class="show servicos__item item_2">
                        <img src="img/image-1.png" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="contactos" id="contactos">
            <div class="container">
                <h2 class="container__title">Contactos</h2>
                <?php
                $action=$_REQUEST['action'];
                if ($action=="")    /* display the contact form */
                    {
                    ?>
                    <div class="wrapper">
                        <div class="form__contactos">
                            <form  action="index.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="submit">
                            <label for="name">Nome:</label>
                            <input id="name" class="form" name="name" type="text" value="" size="30"/><br>
                            <label for="email">Email:</label>
                            <input id="email" class="form" name="email" type="text" value="" size="30"/><br>
                            <label for="message">Mensagem:</label>
                            <textarea id="message" class="form" name="message" rows="7" cols="30"></textarea><br>
                            <input type="submit" value="Enviar email"/>
                            </form>
                        </div>
                    </div>
                    <?php
                    }
                else                /* send the submitted data */
                    {
                    $name=$_REQUEST['name'];
                    $email=$_REQUEST['email'];
                    $message=$_REQUEST['message'];
                    if (($name=="")||($email=="")||($message==""))
                        {
                        echo "All fields are required, please fill <a href=\"index.php#contactos\">the form</a> again.";
                        }
                    else{
                        $from="From: $name<$email>\r\nReturn-path: $email";
                        $subject="Mensagem enviada através do site";
                        mail("geral@theemotionweddingfilms.com", $subject, $message, $from);
                        echo "Email enviado!";
                        }
                    }
                ?>
            </div>
        </section>
        <footer>
            <div class="container">
                <div class="wrapper">
                    <div class="footer__tel">
                        <p><span> + 351 91 780 18 90</span></p>
                    </div>
                    <div class="footer__email">
                        <p>geral[at]theemotionweddingfilms.com</p>
                    </div>
                    <div class="footer__social">
                        <p>dsadadadsadsa</p>
                    </div>
                </div>
                <div class="footer__patrocinio">
                    <a href="http://simplesmentebranco.com" target="_blank"><img src="img/simples.png" alt="Simplesmente Branco" class="icon-simples"></a>
                </div>
            </div>
        </footer>
    </body>
</html>
