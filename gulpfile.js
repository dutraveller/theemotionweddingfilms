// var paths
var sass_path = 'sass/';


var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    //cmq         = require('gulp-combine-media-queries'),
    gcmq        = require('gulp-group-css-media-queries'),
    cssnano     = require('gulp-cssnano'),
    uglify      = require("gulp-uglify"),
    flatten     = require("gulp-flatten"),
    notify      = require('gulp-notify'),
    plumber     = require('gulp-plumber'),
    watch       = require('gulp-watch');

    //inlinecss = require('gulp-inline-css');


// == Sass ==================================
gulp.task('styles', function() {

    //gulp.src([sass_path +'main.scss', sass_path +'main-corporate.scss', sass_path +'ie.scss', sass_path +'ie8.scss', sass_path +'print.scss'])
    gulp.src([sass_path +'main.scss'])

        .pipe(plumber())

        .pipe(sass({
            style: 'expanded',
            onError: function(error) {
                notify({
                  title: "SASS ERROR",
                  message: "line " + error.line + " in " + error.file.replace(/^.*[\\\/]/, '') + "\n" + error.message
                }).write(error);
            }
        }))
        .pipe(gulp.dest('css'))

        //.pipe(cmq({
        //    log: false
        //}))
        .pipe(gcmq())
        .pipe(gulp.dest('css'))

        .pipe(cssnano())
        .pipe(gulp.dest('css'))

        .pipe(notify({
            title: "Gulp sass",
            message: "Great success! <%= file.relative %>"
        }))

});


// == Scripts ==================================
// gulp.task('scripts', function() {
//   return gulp.src('js/**/*.js')
//
//     .pipe(uglify()) // minify code
//     .pipe(flatten()) // move to a unique folder
//     .pipe(gulp.dest('./compressed/'));
// });


gulp.task('watch', function() {

    gulp.watch(sass_path + '**/*.scss', ['styles']);
    // gulp.watch('js/compressed/**/*.js', ['scripts']);
    
    gulp.watch('index.php');
    gulp.watch('index.html');

    //gulp.watch(['./Content/Images/icons/**/*.svg'], ['svgo']);
    //gulp.watch(['./Content/Images/icons/optimized/**/*.svg'], ['sprites']);

    //gulp.watch(['email-*.html'], ['inlinecss']);

});

gulp.task('default', ['watch'], function() {

});
